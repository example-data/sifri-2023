let baseIRI = "https://clre.solirom.ro/";

let dataset =
    `
@base <${baseIRI}> .
@prefix : <https://clre.solirom.ro/> .
@prefix da: <https://solirom-cflr.gitlab.io/data/A100058/> .
@prefix dlr: <https://solirom-cflr.gitlab.io/data/A100059/> .

<abonare> :located_at da:A129494.html .
<abonare> :pos "s. f." .
<abonare> :quote_siglum "URICARIUL" .

<arbore> :located_at da:A104363.html .
<arbore> :pos "s. m." .
<arbore> :quote_siglum "HEM" .

<ciucure> :located_at da:A111719.html .
<ciucure> :pos "s. m." .
<ciucure> :quote_siglum "URICARIUL" .

<oferent> :located_at dlr:A142545.html .
<oferent> :pos "s. m." .
<oferent> :quote_siglum "Gheție, R. M." .

<păsărărie> :located_at dlr:A182291.html .
<păsărărie> :pos "s. f." .
<păsărărie> :quote_siglum "Ghica, S." .

<ponderanță> :located_at dlr:A246628.html .
<ponderanță> :pos "s. f." .
<ponderanță> :quote_siglum "Ghica, A." .

<ofensiv,%20-ă> :located_at dlr:A140780.html .
<ofensiv,%20-ă> :pos "adj." .
<ofensiv,%20-ă> :quote_siglum "Ghica, A." .
`;

let queryEngine = new Comunica.QueryEngine();

document.addEventListener("click", async event => {
    let target = event.target;

    if (target.matches("button#query-1")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select ?siglum (str(count(?headword)) as ?frequency) (group_concat(?headword ;separator=", ") as ?headwords) {
            ?headword :quote_siglum ?siglum .
        }
        group by ?siglum
        order by asc(?siglum)
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let siglum = binding.siglum
                .replaceAll("\"", "");
            let frequency = binding.frequency
                .replaceAll("\"", "");
            let headwords = binding.headwords
                .replaceAll("https://clre.solirom.ro/", "")
                .replaceAll("%20", " ");
            resultDOMString += `${siglum}, ${frequency}, ${headwords}<br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([15, 16, 17, 18, 19, 20, 21]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(8);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;
    }

    if (target.matches("button#query-2")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select (str(?h) as ?headword) ?url {
            ?h :located_at ?url .
            ?h :pos "s. m." .
            filter (regex(str(?h), ".re$", "i"))
        }
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let headword = binding.headword
                .replace("https://clre.solirom.ro/", "")
                .replaceAll("\"", "")
                .replace("%20", " ");
            let url = binding.url;
            resultDOMString += `<a href="${url}" target="_blank">${headword}</a><br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([9, 10]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(4);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-3")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select (str(?h) as ?headword) ?url {
            ?h :located_at ?url .
        }
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let headword = binding.headword
                .replace("https://clre.solirom.ro/", "")
                .replaceAll("\"", "")
                .replace("%20", " ");
            let url = binding.url;
            resultDOMString += `<a href="${url}" target="_blank">${headword}</a><br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([1, 2, 3, 4, 5, 6, 7]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(1);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-4")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select (str(?h) as ?headword) ?url {
            ?h :located_at ?url .
            filter (regex(str(?h), ".re$", "i"))
        }
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let headword = binding.headword
                .replace("https://clre.solirom.ro/", "")
                .replaceAll("\"", "")
                .replace("%20", " ");
            let url = binding.url;
            resultDOMString += `<a href="${url}" target="_blank">${headword}</a><br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([1, 6, 7]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(2);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-5")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select (str(?h) as ?headword) ?url {
            ?h :located_at ?url .
            ?h :pos "s. f." .
        }
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let headword = binding.headword
                .replace("https://clre.solirom.ro/", "")
                .replaceAll("\"", "")
                .replace("%20", " ");
            let url = binding.url;
            resultDOMString += `<a href="${url}" target="_blank">${headword}</a><br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([8, 13, 14]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(3);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-6")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select distinct ?pos {
            ?headword :pos ?pos .
        }
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let pos = binding.pos
                .replaceAll("\"", "");
            resultDOMString += `${pos}<br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([8, 9, 10, 11, 12, 13, 14]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(5);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-7")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select ?pos (str(count(?headword)) as ?frequency) (group_concat(?headword ;separator=", ") as ?headwords) {
            ?headword :pos ?pos .
        }
        group by ?pos
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            binding = JSON.parse(binding.toString());
            let pos = binding.pos
                .replaceAll("\"", "");
            let frequency = binding.frequency
                .replaceAll("\"", "");
            let headwords = binding.headwords
                .replaceAll("https://clre.solirom.ro/", "")
                .replaceAll("%20", " ");
            resultDOMString += `${pos}, ${frequency}, ${headwords}<br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([8, 9, 10, 11, 12, 13, 14]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(6);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

    if (target.matches("button#query-8")) {
        const bindingsStream = await queryEngine.queryBindings(`
        base <https://clre.solirom.ro/>
        prefix : <https://clre.solirom.ro/>
        prefix da: <https://solirom-cflr.gitlab.io/data/A100058/>

        select distinct ?siglum {
            ?headword :quote_siglum ?siglum .
        }
        order by asc(?siglum)
      `, {
            sources: [
                {
                    type: "stringSource",
                    value: dataset,
                    mediaType: "text/turtle",
                    baseIRI: baseIRI,
                },
            ],
        });

        document.querySelector("section#result-items").innerHTML = "";
        let resultDOMString = "";

        let rawBindings = await bindingsStream.toArray();
        for (let binding of rawBindings) {
            console.log(binding.toString());
            binding = JSON.parse(binding.toString());
            let siglum = binding.siglum
                .replaceAll("\"", "");
            resultDOMString += `${siglum}<br/>`;
        }

        // highlight the dataset items
        resetDatasetItemsHighlight();
        highlightDatasetItems([15, 16, 17, 18, 19, 20, 21]);

        // highlight the query section
        resetQuerySectionsHighlight();
        highlightQuerySection(7);

        // display the results
        document.querySelector("section#result-items").innerHTML = resultDOMString;        
    }

});

const resetDatasetItemsHighlight = () => {
    let datasetItems = document.querySelectorAll("#dataset-items > span");
    datasetItems.forEach(item => item.classList.remove("selected"));
};

const highlightDatasetItems = (datasetItemIds) => {
    datasetItemIds.forEach(id => document.querySelector("#dataset-item-" + id).classList.add("selected"));
};

const resetQuerySectionsHighlight = () => {
    let datasetItems = document.querySelectorAll("section.query-container");
    datasetItems.forEach(item => item.classList.remove("selected"));
};

const highlightQuerySection = (position) => {
    let querySection = document.querySelector(`section.query-container:nth-of-type(${position})`);
    console.log(querySection);
    querySection.classList.add("selected");
};

